import networkx as nx
import matplotlib.pyplot as plt


def getBipartiteCliques(aMat, obj, attr, dictBC):
    # 根据形式背景数据获得其中所有概念
    # aMat: 二维列表，形式概念背景数据，对象有属性对应位置为1，否则为0
    # obj: 对象列表
    # attr: 属性列别
    # dictBC: 存储概念的字典，key为一个对象，value为该对象对应的所有属性列表
    cList = []
    aLen = len(aMat)
    bLen = len(aMat[0])
    #    printMat(aMat)
    #   print(aLen, bLen,  " are aLen and bLen\n\n")
    for x in range(0, aLen):
        tmpList = []
        tmpObj = [obj[x]]

        for y in range(0, bLen):
            if aMat[x][y] == '1':
                tmpList.append(attr[y])

        tmp = tmpObj, tmpList
        dictBC[obj[x]] = tmpList
        cList.append(tmp)

    for x in range(0, bLen):
        tmpList = []
        tmpattr = [attr[x]]

        for y in range(0, aLen):
            if aMat[y][x] == '1':
                tmpList.append(obj[y])

        tmp = tmpList, tmpattr
        dictBC[attr[x]] = tmpList
        cList.append(tmp)

    return cList

def condenseList(inputlist):
    # 合并概念，将具有相同对象(属性)的概念合并
    # inputlist: getBipartiteCliques返回的列表
    clist = []
    toSkip = []
    for x in range(0, len(inputlist)):
        if x in toSkip:
            continue
        matched = 0
        for y in range(x + 1, len(inputlist)):
            if y in toSkip:
                continue
            if set(inputlist[x][0]) == set(inputlist[y][0]):
                tmpTuple = inputlist[x][0], list(set(inputlist[x][1]).union(set(inputlist[y][1])))
                clist.append(tmpTuple)
                toSkip.append(y)
                matched = 1
                break
            elif set(inputlist[x][1]) == set(inputlist[y][1]):
                tmpTuple = list(set(inputlist[x][0]).union(set(inputlist[y][0]))), inputlist[x][1]
                clist.append(tmpTuple)
                toSkip.append(y)
                matched = 1
                break
        if matched == 0:
            clist.append(inputlist[x])

    return clist

def removeUnclosed(clist, dictBC):
    # 移除非闭概念
    # clist: condenseList返回的列表
    # dictBC: 存储概念的字典，key为一个对象，value为该对象对应的所有属性列表
    flist = []
    listo = []
    lista = []
    for x in range(0, len(clist)):
        lista = []
        listo = []
        for y in range(0, len(clist[x][0])):
            if lista == []:
                lista = dictBC[clist[x][0][y]]
            else:
                lista = list(set(lista).intersection(set(dictBC[clist[x][0][y]])))

        for z in range(0, len(clist[x][1])):
            if listo == []:
                listo = dictBC[clist[x][1][z]]
            else:
                listo = list(set(listo).intersection(set(dictBC[clist[x][1][z]])))
        #       print ("printing both list for ",  x,  lista,  listo)
        if set(lista) == set(clist[x][1]) and set(listo) == set(clist[x][0]):
            flist.append(clist[x])
    return flist

def generateLattice(bCList, obj, attr, dictBC, hasSuccessor, hasPredecessor):
    # bCList: removeUnclosed返回的经过关键字为intent长度排序后的列表
    # aMat: 二维列表，形式概念背景数据，对象有属性对应位置为1，否则为0
    # obj: 对象列表
    # attr: 属性列别
    # dictBC: 存储概念的字典，key为一个对象，value为该对象对应的所有属性列表
    # hasSuccessor: 列表
    # hasPredecessor: 列表
    G = nx.Graph()
    for x in range(0, len(bCList)):
        nodeName = "".join(str(m) for m in bCList[x][0]) + ", " + "".join(str(m) for m in bCList[x][1])
        G.add_node(nodeName)

    for x in range(0, len(bCList)):
        for y in range(x + 1, len(bCList)):
            if set(bCList[x][0]).issubset(set(bCList[y][0])):
                nodeName1 = "".join(str(m) for m in bCList[x][0]) + ", " + "".join(str(m) for m in bCList[x][1])
                nodeName2 = "".join(str(m) for m in bCList[y][0]) + ", " + "".join(str(m) for m in bCList[y][1])
                G.add_edge(nodeName1, nodeName2)
                hasSuccessor.append(x)
                hasPredecessor.append(y)

    # Creating top most and bottom most node
    listo = []
    lista = []
    for x in range(0, len(attr)):
        if listo == []:
            listo = dictBC[attr[x]]
        else:
            listo = list(set(listo).intersection(set(attr[x])))

    for x in range(0, len(obj)):
        if lista == []:
            lista = dictBC[obj[x]]
        else:
            lista = list(set(lista).intersection(set(obj[x])))
    if lista == []:
        lista = ["null"]
    if listo == []:
        listo = ["null"]

    # adding them to graph
    firstNode = "".join(str(m) for m in listo) + ", " + "".join(str(m) for m in attr)
    G.add_node(firstNode)
    lastNode = "".join(str(m) for m in obj) + ", " + "".join(str(m) for m in lista)
    G.add_node(lastNode)

    # adding edges to them
    for x in range(0, len(bCList)):
        if x not in hasSuccessor:
            nodeName = "".join(str(m) for m in bCList[x][0]) + ", " + "".join(str(m) for m in bCList[x][1])
            G.add_edge(nodeName, lastNode)

    for x in range(0, len(bCList)):
        if x not in hasPredecessor:
            nodeName = "".join(str(m) for m in bCList[x][0]) + ", " + "".join(str(m) for m in bCList[x][1])
            G.add_edge(nodeName, firstNode)
    nx.draw(G)
    plt.savefig("lattice.png")
