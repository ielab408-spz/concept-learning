import numpy
import pandas
import cv2
import scipy.io as scio
from glob import glob


def value_to_scale(value):
    if(0 <= value < 65):
        return 0
    elif(65 <= value < 130):
        return 1
    elif (130 <= value < 195):
        return 2
    else:
        return 3

def images_to_pandas(image_path, file_type):
    data_files = list(numpy.array(glob(image_path + '/*.' + file_type)))
    images_csv = pandas.DataFrame(columns=['image_name', 'image_data'])
    for i in data_files:
        image_data = cv2.imread(i)
        image_file = pandas.DataFrame({'image_name': i.split('/')[-1], 'image_data': image_data}, index=['0'])
        images_csv = images_csv.append(image_file)
    return images_csv
