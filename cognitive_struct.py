# 概念认知数据存储结构
class concept:

    def __init__(self, extent, intent):
        self._ext = extent
        self._int = intent
        self._children = []

    def getext(self):
        return self._ext

    def getint(self):
        return self._int

    def getchildren(self):
        return self._children

    def add_child(self, cpt):
        self._children.append(cpt)

    def go(self, extent):
        for child in self._children:
            if(child.getext() == extent):
                return child
            elif(set(extent).issubset(set(child.getext()))):
                return child
        return None


class cognitive_struct:

    def __init__(self):
        self._head = concept('header', ' ')

    def linktohead(self, cpt):
        self._head.add_child(cpt)

    def insert(self, cpt):
        tmp = cur = self._head
        while (True):
            cur = cur.go(cpt.getext())
            if(cur == None):
                tmp.add_child(cpt)
                break
            elif(cur.getext() == cpt.getext()):
                cur.add_child(cpt)
                break
            tmp = cur

    def search(self, extent):
        cur = self._head
        while(True):
            cur = cur.go(extent)
            if(cur == None):
                break
            elif(cur.getext() == extent):
                break
        return cur

    def DFS(self, i, statue, result, cpt):
        # i: 存储深度搜索中当前在第几个认知状态
        # statue: 想要获取的认知状态
        # result: 存储statue认知状态中所有概念
        # concept: 起始概念
        if(concept):
            if(i == statue):
                result.append(cpt)
            else:
                for cp in cpt.getchildren():
                    self.DFS(i + 1, statue, result, cp)

    def find_father_obj(self, extent, concept_list):
        # 找出所有外延包含extent的概念
        # concept_list: 当前认知状态所对应的概念列表
        fathers = []
        for cur in concept_list:
            if(set(extent).issubset(set(cur.getext()))):
                fathers.append(cur)
        return fathers

    def find_child_obj(self, extent, concept_list):
        # 找出所有extent包含的外延的概念
        childs = []
        for cur in concept_list:
            if(set(cur.getext()).issubset(set(extent))):
                childs.append(cur)
        return childs

    def find_child_attr(self, intent, concept_list):
        # 找出所有intent包含的内涵的概念
        childs = []
        for cur in concept_list:
            if(set(cur.getint()).issubset(set(intent))):
                childs.append(cur)
        return childs

    def find_father_attr(self, intent, concept_list):
        # 找出所有内涵包含intent的概念
        fathers = []
        for cur in concept_list:
            if(set(intent).issubset(set(cur.getint()))):
                fathers.append(cur)
        return fathers

    def learning_acc(self, type, lower_obj, upper_obj, lower_attr, upper_attr, U, A, mat):
        # type: # 默认是全体, "obj", "attr"
        # lower_obj: 对象的下确界列表
        # upper_obj: 对象的上确界列表
        # lower_attr: 属性的下确界列表
        # upper_attr: 属性的上确界列表
        # U: 全体对象的列表
        # A: 全体属性列表
        # mat: 字典类型，当前认知状态对应的形式背景数据关系，关键字为每个对象或者属性，值为关键字拥有的属性，
        #       ie. mat[1] = ['a', 'b', 'd', 'f'], mat[a] = [1, 3, 5]
        if(type == 'obj'):
            return 1 - (len(upper_obj) - len(lower_obj)) / len(U)
        elif(type == 'attr'):
            return 1 - (len(lower_attr) - len(upper_attr)) / len(A)
        else:
            return 1 - (((len(upper_obj) - len(self.H(mat, lower_attr))) / 2 * len(U)) + ((len(lower_attr) - len(self.L(mat, upper_obj))) / 2 * len(A)))


    def L(self, mat, obj):
        # mat: 字典类型，当前认知状态对应的形式背景数据关系，关键字为每个对象或者属性，值为关键字拥有的属性，
        #       ie. mat[1] = ['a', 'b', 'd', 'f'], mat[a] = [1, 3, 5]
        # obj: 进行L操作的对象列表
        result = set(mat[obj[0]])
        for i in range(1, len(obj)):
            result.intersection_update(set(mat[obj[i]]))
        return list(result)

    def H(self, mat, attr):
        # mat: 字典类型，当前认知状态对应的形式背景数据关系，关键字为每个对象或者属性，值为关键字拥有的属性，
        #       ie. mat[1] = ['a', 'b', 'd', 'f'], mat[a] = [1, 3, 5]
        # attr: 进行H操作的属性列表
        result = set(mat[attr[0]])
        for i in range(1, len(attr)):
            result.intersection_update(set(mat[attr[i]]))
        return list(result)


a = concept([1, 2, 3, 4, 5, 6, 7], [])
b = concept([1, 2, 3], ['b'])
c = concept([1, 2, 4], ['a'])
d = concept([2, 4, 6], ['d'])
e = concept([3, 4], ['e'])
f = concept([1, 3, 5], ['c'])
g = concept([7], ['h'])
h = concept([1, 2], ['a', 'b'])
i = concept([1, 3], ['b', 'c'])
j = concept([1], ['a', 'b', 'c'])
k = concept([], ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'])
cs = cognitive_struct()
cs.linktohead(a)
cs.insert(b)
cs.insert(c)
cs.insert(d)
cs.insert(e)
cs.insert(f)
cs.insert(g)
cs.insert(h)
cs.insert(i)
cs.insert(j)
cs.insert(k)

